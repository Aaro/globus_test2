//
//  MyTitleCell.h
//  Globus_Test2
//
//  Created by Кирилл Шальнов on 15.05.16.
//  Copyright © 2016 Надежда Шальнова. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTitleCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *Title;
@property (weak, nonatomic) IBOutlet UILabel *Subtitle;

@end

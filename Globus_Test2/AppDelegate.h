//
//  AppDelegate.h
//  Globus_Test2
//
//  Created by Кирилл Шальнов on 14.05.16.
//  Copyright © 2016 Надежда Шальнова. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


//
//  ViewController.h
//  Globus_Test2
//
//  Created by Кирилл Шальнов on 15.05.16.
//  Copyright © 2016 Надежда Шальнова. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyCommission.h"
@interface ViewController : UIViewController

@property(strong) MyCommission* MCProperty;
@property (weak, nonatomic) IBOutlet UILabel *TitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *SubtitleLabel;

@end

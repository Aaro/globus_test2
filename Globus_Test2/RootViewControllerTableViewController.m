//
//  RootViewControllerTableViewController.m
//  Globus_Test2
//
//  Created by Кирилл Шальнов on 14.05.16.
//  Copyright © 2016 Надежда Шальнова. All rights reserved.
//

#import "RootViewControllerTableViewController.h"
#import "MyTitleCell.h"
#import "ViewController.h"
#import "MyCommission.h"





@implementation RootViewControllerTableViewController


#pragma mark - Table view data source

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayCommissions = [[NSMutableArray alloc] init];  //NSArray *name =
    records = [Record MR_findAllSortedBy:@"number" ascending:YES];
    
    for (id record in records) {
        Record * rc = record;
        NSLog(@"Title: %@", rc.title);
        NSLog(@"Number: %@", rc.number);
        
        MyCommission* mc = [[MyCommission alloc] init];
        mc.TitleA = rc.title;
        mc.SubTitleA = rc.subtitle;
        [arrayCommissions addObject:mc];
    }
    countCommissions = records.count;
    self.navigationItem.title = @"Table";
    UIBarButtonItem* editButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(actionEdit:)];
    UIBarButtonItem* addButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(actionAdd:)];
    self.navigationItem.rightBarButtonItem = editButton;
    self.navigationItem.leftBarButtonItem = addButton;
}


#pragma mark - Actions

-(void) actionEdit:(UIBarButtonItem*) sender {
    /* UIBarButtonSystemItem item = UIBarButtonSystemItemDone;
     UIBarButtonItem* editButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:item
     target:self
     action:@selector(actionEdit:)];
     self.navigationItem.rightBarButtonItem = editButton;
     */
    BOOL isEditing = self.tableView.editing;
    [self.tableView setEditing:!isEditing animated:YES];
    UIBarButtonSystemItem item = UIBarButtonSystemItemEdit;
    if (self.tableView.editing) {
        item = UIBarButtonSystemItemDone;
    }
    UIBarButtonItem* editButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:item  target:self action:@selector(actionEdit:)];
    [self.navigationItem setRightBarButtonItem:editButton animated:YES];
}


-(void) actionAdd:(UIBarButtonItem*) sender {
    MyCommission* mc = [[MyCommission alloc] init];
    mc.TitleA = [NSString stringWithFormat:@" Поручение %lu", countCommissions + 1];
    mc.SubTitleA = [NSString stringWithFormat:@" Описание поручения %lu", countCommissions + 1];
    [arrayCommissions addObject:mc];
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        Record *rc = [Record MR_createEntityInContext:localContext];
        rc.number = [NSNumber numberWithInt:countCommissions];
        rc.title = mc.TitleA;
        rc.subtitle = mc.SubTitleA;
    }];
    countCommissions++;
    [self.tableView reloadData];
}


// Do any additional setup after loading the view, typically from a nib.


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -  UITableViewDataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSLog(@"numberOfSectionsInTableView");
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"numberOfRowsInSectionView %ld", (long)section);
    return arrayCommissions.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyCommission* mc = arrayCommissions[indexPath.row];
    MyTitleCell *Cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    Cell.Title.text = mc.TitleA;
    Cell.Subtitle.text = mc.SubTitleA;
    return Cell;
    
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"myView"];
    myView.MCProperty = arrayCommissions[indexPath.row];
    [self.navigationController pushViewController:myView animated:YES];
}


-(void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(editingStyle == UITableViewCellEditingStyleDelete){
        MyCommission* mcDelete = arrayCommissions[indexPath.row];
        [arrayCommissions removeObject:mcDelete];
        [tableView beginUpdates];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [tableView endUpdates];
        [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
            [records[indexPath.row] MR_deleteEntityInContext:localContext];
        }];
    }
}


#pragma mark - UITableViewDelegate
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}


-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return YES;
}


- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    if (fromIndexPath != toIndexPath ) {
        
        MyCommission* mc = arrayCommissions[fromIndexPath.row];
        [arrayCommissions removeObjectAtIndex:fromIndexPath.row];
        [arrayCommissions insertObject:mc atIndex:toIndexPath.row];
        
        Record * recordFromIndexPath = records[fromIndexPath.row];
        
        //[MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        [RootViewControllerTableViewController updateIndex:fromIndexPath.row: toIndexPath.row];
        //}];
        [tableView reloadData];
    }
}


+(void) updateIndex: (NSUInteger) fromIndexPathIndex:(NSUInteger) toIndexPathIndex {
    NSArray* records = [Record MR_findAll];
    
    if (fromIndexPathIndex > toIndexPathIndex) {
        for (id record in records){
            Record *rc = record;
            if ([rc.number intValue] >= toIndexPathIndex && [rc.number intValue] < fromIndexPathIndex) {
                [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                    Record *localRecord = [rc MR_inContext:localContext];
                    localRecord.number = [NSNumber numberWithInt: [rc.number intValue] + 1];
                    NSLog(@"Title: %@", rc.title);
                    NSLog(@"Number: %@", rc.number);
                }];
            }
        }
    }
    else {
        for (id record in records){
            Record *rc = record;
            if ([rc.number intValue] <= toIndexPathIndex && [rc.number intValue] > fromIndexPathIndex ) {
                [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                    Record *localRecord = [rc MR_inContext:localContext];
                    localRecord.number = [NSNumber numberWithInt: [rc.number intValue] -1];
                    NSLog(@"Title: %@", rc.title);
                    NSLog(@"Number: %@", rc.number);
                }];
                
            }
        }
    }
    
    Record *rc = records[fromIndexPathIndex];
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        Record *localRecord = [rc MR_inContext:localContext];
        localRecord.number = [NSNumber numberWithInt: toIndexPathIndex];
    }];

}

@end

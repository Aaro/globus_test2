//
//  RootViewControllerTableViewController.h
//  Globus_Test2
//
//  Created by Кирилл Шальнов on 14.05.16.
//  Copyright © 2016 Надежда Шальнова. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MagicalRecord/MagicalRecord.h>
#define MR_SHORTHAND
#import "Record.h"



@interface RootViewControllerTableViewController : UITableViewController
{
    NSMutableArray* arrayCommissions;
    NSUInteger countCommissions;
    NSArray *records;
}

@end

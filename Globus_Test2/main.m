//
//  main.m
//  Globus_Test2
//
//  Created by Кирилл Шальнов on 14.05.16.
//  Copyright © 2016 Надежда Шальнова. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

//
//  MyCommission.h
//  Globus_Test2
//
//  Created by Кирилл Шальнов on 15.05.16.
//  Copyright © 2016 Надежда Шальнова. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyCommission : NSObject

@property(strong) NSString *TitleA;
@property(strong) NSString *SubTitleA;

@end

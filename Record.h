//
//  Record.h
//  globus_test2
//
//  Created by Nadezhda on 23.06.16.
//  Copyright © 2016 Надежда Шальнова. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Record : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Record+CoreDataProperties.h"

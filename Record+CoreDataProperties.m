//
//  Record+CoreDataProperties.m
//  globus_test2
//
//  Created by Nadezhda on 23.06.16.
//  Copyright © 2016 Надежда Шальнова. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Record+CoreDataProperties.h"

@implementation Record (CoreDataProperties)

@dynamic number;
@dynamic subtitle;
@dynamic title;

@end
